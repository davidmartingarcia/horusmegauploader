import subprocess
import inquirer
import datetime
from mega import Mega
import sys
import os

#Depends inquirer - mega.py - pycrypto && python dev packages - python-dev and libpython-dev

def listVMs():
    p = subprocess.Popen("VBoxManage list vms", stdout = subprocess.PIPE, shell = True)
    (output, err) = p.communicate()
    p.wait()
    return  output.split("\n")

def exportVM(name, url):
    try:
        p = subprocess.Popen("VBoxManage export " + name + " -o " + url, stdout = subprocess.PIPE, shell = True)
        if p.wait() != 0:
            print "Error in OVA-file"
    except KeyboardInterrupt:
            p.kill()
            sys.exit(0)

def uploadMega(email, passwd, file):
    try:
        mega = Mega({'verbose': True})
        m = mega.login(email, passwd)
        print "Mega.co.nz -> login Done!\nUploading!"
        file = m.upload(file)
        return m.get_upload_link(file)
    except Exception as e:
        print "Login Error" + str(e.args)
        return -1

def getParameters():
    #Login and VM?
    userQuestions = [inquirer.Text('user', message = "What's your user/mail: default \"anonymous\""),inquirer.Password('pass', message = "What's your password"),inquirer.List('vm', message = "What VM do you need?", choices = listVMs())]

    answers = inquirer.prompt(userQuestions)
    return answers['user'], answers['pass'], answers['vm'].split("\"")[1]

def removeFile(file = None):
    return os.remove(file)


#Initial Message and Chech parameters loop

print "__      ______            "
print "\ \    / /  _ \           "
print " \ \  / /| |_) | _____  __"
print "  \ \/ / |  _ < / _ \ \/ /"
print "   \  /  | |_) | (_) >  < "
print "    \/   |____/ \___/_/\_\ "
print " __  __                  _    _       _                 _ "
print "|  \/  |                | |  | |     | |               | |"
print "| \  / | ___  __ _  __ _| |  | |_ __ | | ___   __ _  __| |"
print "| |\/| |/ _ \/ _` |/ _` | |  | | '_ \| |/ _ \ / _` |/ _` |"
print "| |  | |  __/ (_| | (_| | |__| | |_) | | (_) | (_| | (_| |"
print "|_|  |_|\___|\__, |\__,_|\____/| .__/|_|\___/ \__,_|\__,_|"
print "              __/ |            | | "
print "             |___/             |_| "

#Menu control
isVMGenerated = False
isCorrect = False
while isCorrect != True:

    megaUser,megaPass,selectedVM = getParameters()

    print "Should I continue uploading to Mega.co.nz:\nUser:"+megaUser+"\nPass:"+ "*" * len(megaPass) + "\nVM:"+selectedVM

    #Continue?
    uploadQuestions = [
        inquirer.Confirm('correct',message = "Is correct?", default = True)
    ]
    answers = inquirer.prompt(uploadQuestions)
    isCorrect = answers['correct']

if not(isVMGenerated):
    #Export VM
    file = selectedVM + "-" + datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")  + ".ova"
    print "Building OVA-file: " + file
    exportVM(selectedVM, file)
    isVMGenerated = True

    #Upload
    print "Uploading to Mega.co.nz"
    url = uploadMega(megaUser, megaPass, file)
    if url != -1:
        print "File URL - " + url
        isCorrect = False

print "Purge all data"
removeFile(file)
