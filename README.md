# HorusMegaUploader #

Script en python creado para el mantenimiento de la maquina virtual de desarrollo del proyecto Horus. Perfecto para hacer backups periodicos.

### ¿Que hace? ###

* Crea la OVA de la maquina virtual que se elije en el menu.
* La sube a la cuenta de mega que se introduzca

### ¿Posibles mejoras? ###

* Descargar e instalar maquinas
* Apagado de la maquina
* Un sin fin mas de cosas.

### Contribuciones ###

* Mail a base.dks (arroba) gmail (dotcom)
